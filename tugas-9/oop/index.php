<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
        require_once("Ape.php");
        require_once("Frog.php");
        
        $sheep = new Animal("shaun");

        echo $sheep->name . '<br>'; // "shaun"
        echo $sheep->legs . '<br>'; // 4
        echo $sheep->cold_blooded . '<br>'; // "no"

        $sungokong = new Ape("kera sakti");

        $kodok = new Frog("buduk");

        echo "<br>outpus hasil : <br>"; 
        echo "Name : " . $sheep->name . '<br>';
        echo "Legs : " . $sheep->legs . '<br>';
        echo "cold blooded : " . $sheep->cold_blooded . '<br><br>';

        echo "Name : " . $kodok->name . '<br>';
        echo "Legs : " . $kodok->legs . '<br>';
        echo "cold blooded : " . $kodok->cold_blooded . '<br>';
        echo "Jump : "; $kodok->jump();
        echo '<br><br>';

        echo "Name : " . $sungokong->name . '<br>';
        echo "Legs : " . $sungokong->legs . '<br>';
        echo "cold blooded : " . $sungokong->cold_blooded . '<br>';
        echo "Yell : "; $sungokong->yell();
        echo '<br>';
    ?>
</body>
</html>