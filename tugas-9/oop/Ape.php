<?php
    require_once("Animal.php");
    class Ape extends Animal {
        public function __construct($name) {
            parent::__construct($name); // Memanggil constructor dari parent class
            $this->legs = 2;
        }
        public function yell() {
            echo "Aouuu";
        }
    }
?>