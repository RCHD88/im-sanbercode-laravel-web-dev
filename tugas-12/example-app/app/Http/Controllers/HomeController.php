<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function main(){
        return view('home');
    }
    public function fillform(){
        return view('register');
    }
    public function sendmessage(Request $request){
        $firstname = $request['firstname'];
        $lastname = $request['lastname'];
        
        return view('welcome', ['firstname' => $firstname, 'lastname' => $lastname]);
    }
}
