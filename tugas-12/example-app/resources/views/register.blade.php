<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method = "post">
        @csrf 
        <label for="firstname">First name: </label><br><br>
        <input name="firstname"type="text"><br><br>
        
        <label for="lastname">Last name: </label><br><br>
        <input name="lastname" type="text"><br><br>
        
        <label for="gender">Gender: </label><br><br>
        <input type="radio" name="gender" id="male" value="Male"> <label for="male">Man</label> <br>
        <input type="radio" name="gender" id="female" value="Female"> <label for="female">Woman</label> <br>
        <input type="radio" name="gender" id="other" value="Other"> <label for="other">Other</label> <br><br>

        <label for="nationality">Nationality:</label>
        <select name="nationality" id="nationality">
            <option value="Surabaya">Indonesia</option>
            <option value="Jakarta">Malaysia</option>
            <option value="Jakarta">Singapore</option>
        </select>
        <br><br>

        <label for="language">Language Spoken:</label><br>
        <input type="checkbox" name="Bahasa_Indonesia" id="bahasa1">
        <label for="bahasa1">Bahasa Indonesia</label><br>
        <input type="checkbox" name="English" id="bahasa2">
        <label for="bahasa2">English</label><br>
        <input type="checkbox" name="other" id="bahasa3">
        <label for="bahasa3">Arabic</label><br>
        <input type="checkbox" name="other" id="bahasa3">
        <label for="bahasa3">Japanese</label><br>

        <p>Bio:</p>
        <textarea name="bio" id="bio" cols="30" rows="10"></textarea><br>

        <button type="submit">Sign Up</button>
    </form>
</body>
</html>